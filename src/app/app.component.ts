import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { StorageService } from './services/storage/storage.service';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'TaxiPlanner';
  show: boolean = false;

  constructor(
    location: Location,
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService
  ) {
    router.events.subscribe((val) => {
      this.show = location.path() != '' ? true : false;
    });
  }

  ngOnInit() {
    // Redirect user if cookie is valid
    if (this.authService.isAuthenticated() && window.location.pathname == '/') {
      const role = this.storageService.getCookie('taxi_role');
      if (
        role == 'employee' ||
        role == 'delegated_approver' ||
        role == 'approver'
      ) {
        this.router.navigate(['/mybookings']);
      } else if (role == 'hr' || role == 'superadmin') {
        this.router.navigate(['/dashboard']);
      }
    }
  }
}
